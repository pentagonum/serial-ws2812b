#define FASTLED_ALLOW_INTERRUPTS 0
#include <ESP8266WiFi.h>
#include "FastLED.h"

#define NUM_LEDS 100
#define ARTNET_UNIVERSE 11

CRGB leds[NUM_LEDS];
void setup() {
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  
  FastLED.addLeds<NEOPIXEL, D2>(leds, NUM_LEDS);
  FastLED.clear();  
  FastLED.show();
  
  Serial.begin(460800);
  Serial.setTimeout(10000000);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

uint8_t buf[NUM_LEDS * 3];
void loop() {
  Serial.readBytes(buf, NUM_LEDS * 3);
  for (int l = 0; l < NUM_LEDS; l++) {
    leds[l] = CRGB(buf[l * 3], buf[l * 3 + 1], buf[l * 3 + 2]);
  }
  FastLED.show();
  while(Serial.available() > 0) { Serial.read(); } // flush serial buffer
  Serial.print((char)ARTNET_UNIVERSE); // send id/universe
  delay(5);
  }
