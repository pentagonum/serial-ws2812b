# Serial-WS2812b

Communicate with [artnet-serial](https://gitlab.com/pentagonum/artnet-serial).

Send the Art-Net universe we want to subscribe to and receive data which is directly routed to a connected WS2812b on Pin 5 (in our hardware revision of the ESP8266).
